import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Meal', url: '/meal-list', icon: 'list-circle' },
    { title: 'Food datas', url: '/folder/Outbox', icon: 'pizza' },
    { title: 'Config', url: '/folder/Favorites', icon: 'settings' },
    { title: 'Account', url: '/folder/Archived', icon: 'cloud' },
    { title: 'Problems ?', url: '/folder/Trash', icon: 'warning' },
    { title: 'Evaluate', url: '/folder/Spam', icon: 'star' },
    { title: 'Contact', url: '/folder/Spam', icon: 'mail' },
  ];
  constructor() {}

  toggleTheme(event: any) {
    if(event.detail.checked){
      document.body.setAttribute("color-theme", "dark")
    } else {
      document.body.setAttribute("color-theme", "light")
    }
  }
}
