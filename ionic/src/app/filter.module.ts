import {NgModule} from '@angular/core';

import { CommonModule } from '@angular/common';
import { TotalValuesOfSetFilterPipe } from './_filter/totalValueOfSet.filter';
import { FirstLetterFilterPipe } from './_filter/firstLetter.filter.';

@NgModule({
  imports: [ 
    CommonModule 
  ],
  exports: [
    TotalValuesOfSetFilterPipe,
    FirstLetterFilterPipe
  ],
  declarations: [
    TotalValuesOfSetFilterPipe,
    FirstLetterFilterPipe
  ]
})
export class FilterModule {}