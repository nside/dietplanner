import { Component, OnInit } from '@angular/core';
import { NavController } from "@ionic/angular";
import { InfiniteScrollCustomEvent } from '@ionic/angular';

@Component({
  selector: 'app-meal-list',
  templateUrl: './meal-list.page.html',
  styleUrls: ['./meal-list.page.scss'],
})
export class MealListPage implements OnInit {
  public mealList: any = [];

  constructor(
    private navCtrl: NavController,
  ) { 
    
  }

  ngOnInit() {
    this.generateItems();
  }

  public openItem(planId: string): void {
    this.navCtrl.navigateForward(["meal-list/meal-tab", planId,'content'])
  }

  private generateItems() {
    setTimeout(()=> {
      const count = this.mealList.length + 1;
      for (let i = 0; i < 20; i++) {
        this.mealList.push({name: `Plan ${count + i}`, lastUpdate: new Date().toLocaleString(), notification: true})
      }
    }, 2000)
  }

  public onIonInfinite(ev: any) {
    this.generateItems();
    setTimeout(() => {
      (ev as InfiniteScrollCustomEvent).target.complete()
    }, 5000);
  }
}
