import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MealTabnavPage } from './meal-tabnav.page';

const routes: Routes = [
  {
    path: '',
    component: MealTabnavPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MealTabnavPageRoutingModule {}
