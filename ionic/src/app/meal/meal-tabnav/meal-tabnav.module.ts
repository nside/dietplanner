import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';

import { MealTabnavPageRoutingModule } from './meal-tabnav-routing.module';
import { MealTabnavPage } from './meal-tabnav.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: ':planId',
    component: MealTabnavPage,
    children: [
      {
        path: 'content',
        loadChildren: () => import('../meal-content/meal-content.module').then(m => m.MealContentPageModule)
      },
      {
        path: 'details',
        loadChildren: () => import('../meal-details/meal-details.module').then(m => m.MealDetailsPageModule)
      },
      {
        path: 'stats',
        loadChildren: () => import('../meal-stats/meal-stats.module').then(m => m.MealStatsPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/meal-tab/content',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MealTabnavPageRoutingModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MealTabnavPage]
})
export class MealTabnavPageModule {}
