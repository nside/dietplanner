import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexNonAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexPlotOptions,
  ApexResponsive,
  ApexLegend,
  ApexTitleSubtitle,
  ApexStroke,
} from "ng-apexcharts";

export type ChartOptionsPieCharts = {
  title: ApexTitleSubtitle
  series: ApexNonAxisChartSeries
  chart: ApexChart
  legend: ApexLegend
  labels: string[]
  responsive: ApexResponsive[]
  colors: string[]
  dataLabels: ApexDataLabels
  plotOptions: ApexPlotOptions
  stroke: ApexStroke
};

@Component({
  selector: 'app-meal-details',
  templateUrl: './meal-details.page.html',
  styleUrls: ['./meal-details.page.scss'],
})
export class MealDetailsPage implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptionsMacro: ChartOptionsPieCharts;

  constructor() { 
    this.chartOptionsMacro = {
      title: {
        text: "Meals Glucides Distribution"
      },
      series: [579, 230, 90],
      chart: {
        type: "donut",
        height: "350px",
      },
      plotOptions: {
        pie: {
          donut: {
            size: '40%',
            labels: {
              show: true,
              name: {
                offsetY: 0,
              },
              value: {
                fontSize: '14px',
                offsetY: 0,
                color: "var(--color-nutri-fibre)",
              },
              total: {
                label: 'Fibre',
                show: true,
                showAlways: true,
                fontSize: '14px',
                fontWeight: 700,
                color: "var(--color-nutri-fibre)",
                formatter: function (val) {
                  return "120"
                }
              }
            }
          }
        }
      },
      labels: ["Glucide","Proteine","Lipide"],
      responsive: [
        {
          breakpoint: 720,
          options: {
            chart: {
              height: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ],
      legend: {
        show: false,
      },
      colors: [
        "var(--color-nutri-glucide)", 
        "var(--color-nutri-protein)",
        "var(--color-nutri-lipid)",
      ],
      dataLabels: {
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return w.config.series[seriesIndex]
        }
      },
      stroke: {
        width: 0
      }
    }
  }

  ngOnInit() {
  }

}
