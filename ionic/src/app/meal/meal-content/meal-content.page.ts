import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-meal-content',
  templateUrl: './meal-content.page.html',
  styleUrls: ['./meal-content.page.scss'],
})
export class MealContentPage implements OnInit {
  public presentingElement:any = undefined
  constructor(private actionSheetCtrl: ActionSheetController) {

  }

  ngOnInit() {
    this.presentingElement = document.querySelector('.ion-page')
  }

  canDismiss = async () => {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Are you sure?',
      buttons: [
        {
          text: 'Yes',
          role: 'confirm',
        },
        {
          text: 'No',
          role: 'cancel',
        },
      ],
    });

    actionSheet.present();

    const { role } = await actionSheet.onWillDismiss();

    return role === 'confirm';
  };

}
