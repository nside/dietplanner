import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MealContentPage } from './meal-content.page';

const routes: Routes = [
  {
    path: '',
    component: MealContentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MealContentPageRoutingModule {}
