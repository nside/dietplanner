import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MealContentPageRoutingModule } from './meal-content-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MealContentPage } from './meal-content.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    MealContentPageRoutingModule
  ],
  declarations: [MealContentPage]
})
export class MealContentPageModule {}
