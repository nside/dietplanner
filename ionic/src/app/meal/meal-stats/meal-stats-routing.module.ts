import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MealStatsPage } from './meal-stats.page';

const routes: Routes = [
  {
    path: '',
    component: MealStatsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MealStatsPageRoutingModule {}
