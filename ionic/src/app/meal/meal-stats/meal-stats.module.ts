import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { NgApexchartsModule } from "ng-apexcharts";

import { MealStatsPageRoutingModule } from './meal-stats-routing.module';

import { MealStatsPage } from './meal-stats.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgApexchartsModule,
    MealStatsPageRoutingModule
  ],
  declarations: [MealStatsPage]
})
export class MealStatsPageModule {}
