import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexNonAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexResponsive,
  ApexXAxis,
  ApexYAxis,
  ApexLegend,
  ApexFill,
  ApexTitleSubtitle,
  ApexStroke,
  ApexTooltip,
} from "ng-apexcharts";

export type ChartOptionsBarCharts = {
  title: ApexTitleSubtitle
  series: ApexAxisChartSeries
  chart: ApexChart
  xaxis: ApexXAxis
  plotOptions?: ApexPlotOptions
  responsive?: ApexResponsive[]
  yaxis: ApexYAxis | ApexYAxis[]
  legend?: ApexLegend
  fill?: ApexFill
  dataLabels?: ApexDataLabels
  stroke?: ApexStroke
  tooltip?: ApexTooltip
  colors: string[]
};

export type ChartOptionsPieCharts = {
  title: ApexTitleSubtitle
  series: ApexNonAxisChartSeries
  chart: ApexChart
  legend: ApexLegend
  labels: string[]
  totalSeries: number
  responsive: ApexResponsive[]
  colors: string[]
  dataLabels: ApexDataLabels
  plotOptions: ApexPlotOptions
  stroke: ApexStroke
};

@Component({
  selector: 'app-meal-stats',
  templateUrl: './meal-stats.page.html',
  styleUrls: ['./meal-stats.page.scss'],
})
export class MealStatsPage implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptionsStacked: ChartOptionsBarCharts;
  public chartOptionsAgregate: ChartOptionsBarCharts;
  public chartOptionsFatDistribution: ChartOptionsPieCharts;
  public chartOptionsSugarDistribution: ChartOptionsPieCharts;
  public slideOpt = {
    direction: 'horizontal',
    slidesPerView: 1,
    pagination: {
      el: '.swiper-pagination',
    }
  }

  constructor() {
    this.chartOptionsStacked = {
      title: {
        text: "Meal distribution"
      },
      series: [
        {
          name: "Glucides",
          data: [44, 55, 41, 67, 22, 43, 44, 55, 41, 67, 22, 43]
        },
        {
          name: "Proteines",
          data: [13, 23, 20, 8, 13, 27, 13, 23, 20, 8, 13, 27]
        },
        {
          name: "Lipides",
          data: [11, 17, 15, 15, 21, 14, 11, 17, 15, 15, 21, 14]
        }
      ],
      chart: {
        type: "bar",
        height: "350px",
        stacked: true,
        toolbar: {
          show: true
        },
        foreColor: "var(--ion-text-color)",
        zoom: {
          enabled: false
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      xaxis: {
        type: "category",
        categories: [
          "Meal 1",
          "Meal 2",
          "Meal 3",
          "Meal 4",
          "Meal 5",
          "Meal 6",
          "Meal 7",
          "Meal 8",
          "Meal 9",
          "Meal 10",
          "Meal 11",
          "Meal 12",
        ]
      },
      yaxis: [{}],
      legend: {
        position: "right",
        offsetY: 0
      },
      fill: {
        opacity: 1
      },
      colors: [
        "var(--color-nutri-glucide)", 
        "var(--color-nutri-protein)",
        "var(--color-nutri-lipid)",
      ]
    };
    this.chartOptionsAgregate = {
      series: [
        {
          name: "Glucides",
          type: "column",
          data: [44, 55, 41, 67, 22, 43, 44, 55, 41, 67, 22, 43]
        },
        {
          name: "Proteines",
          type: "column",
          data: [13, 23, 20, 8, 13, 27, 13, 23, 20, 8, 13, 27]
        },
        {
          name: "Lipides",
          type: "column",
          data: [11, 17, 15, 15, 21, 14, 11, 17, 15, 15, 21, 14]
        },
        {
          name: "Agg'Glucides",
          type: "line",
          data: [44, 99, 140, 207, 229, 272, 325, 345, 398, 436, 544, 586]
        },
        {
          name: "Agg'Proteines",
          type: "line",
          data: [13, 28, 52, 75, 90, 102, 125, 145, 168, 184, 199, 208]
        },
        {
          name: "Agg'Lipides",
          type: "line",
          data: [11, 27, 35, 58, 75, 85, 102, 125, 134, 148, 155, 171]
        }
      ],
      chart: {
        height: "350px",
        type: "line",
        stacked: false,
        foreColor: "var(--ion-text-color)",
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      title: {
        text: "Meal composition aggregate",
        align: "left",
      },
      xaxis: {
        categories: ["Meal 1", "Meal 2", "Meal 3", "Meal 4", "Meal 5", "Meal 6", "Meal 7", "Meal 8", "Meal 9", "Meal 10", "Meal 11", "Meal 12"]
      },
      yaxis: [{
        seriesName: "Glucides",
        title: {
          text: "Macro"
        }
      },{
        seriesName: "Glucides",
        show: false,
      },{
        seriesName: "Glucides",
        show: false,
      },{
        seriesName: "Agg'Glucides",
        opposite: true,
        title: {
          text: "Aggregate",
        }
      },{
        seriesName: "Agg'Glucides",
        show: false,
      },{
        seriesName: "Agg'Glucides",
        show: false,
      }],
      colors: [
        "var(--color-nutri-glucide)", 
        "var(--color-nutri-protein)",
        "var(--color-nutri-lipid)",
        "var(--color-nutri-glucide-agg)",
        "var(--color-nutri-protein-agg)",
        "var(--color-nutri-lipid-agg)",
      ]
    }

    this.chartOptionsFatDistribution = {
      title: {
        text: "Meals Fat Distribution"
      },
      series: [44, 55, 13, 10],
      totalSeries: [44, 55, 13, 10].reduce((partialSum, a) => partialSum + a, 0),
      chart: {
        type: "donut",
        height: "350px"
      },
      plotOptions: {
        pie: {
          donut: {
            size: '40%',
            labels: {
              show: true,
              name: {
                offsetY: 0,
              },
              value: {
                fontSize: '14px',
                offsetY: 0,
                color: "var(--ion-text-color)",
              },
              total: {
                show: true,
                showAlways: true,
                fontSize: '16px',
                color: "var(--ion-text-color)"
              }
            }
          }
        }
      },
      labels: ["Saturé", "Polyinsaturé", "Monoinsaturé", "Trans"],
      responsive: [
        {
          breakpoint: 720,
          options: {
            chart: {
              height: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ],
      legend: {
        show: false,
      },
      colors: [
        "var(--color-nutri-sature)", 
        "var(--color-nutri-polyinsature)",
        "var(--color-nutri-monoinsature)",
        "var(--color-nutri-trans)",
      ],
      dataLabels: {
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return w.config.series[seriesIndex]
        }
      },
      stroke: {
        width: 0
      }
    }

    this.chartOptionsSugarDistribution = {
      title: {
        text: "Meals Glucides Distribution"
      },
      series: [44, 55, 13, 15, 25, 15, 25, 85],
      totalSeries: [44, 55, 13, 15, 25, 15, 25, 85].reduce((partialSum, a) => partialSum + a, 0),
      chart: {
        type: "donut",
        height: "350px",
      },
      plotOptions: {
        pie: {
          donut: {
            size: '40%',
            labels: {
              show: true,
              name: {
                offsetY: 0,
              },
              value: {
                fontSize: '14px',
                offsetY: 0,
                color: "var(--ion-text-color)",
              },
              total: {
                show: true,
                showAlways: true,
                fontSize: '16px',
                color: "var(--ion-text-color)"
              }
            }
          }
        }
      },
      labels: ["Sucre", "Fructose", "Galactose", "Glucose", "Lactose", "Maltose", "Saccharose", "Amidon"],
      responsive: [
        {
          breakpoint: 720,
          options: {
            chart: {
              height: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ],
      legend: {
        show: false,
      },
      colors: [
        "var(--color-nutri-sucre)", 
        "var(--color-nutri-fructose)",
        "var(--color-nutri-galactose)",
        "var(--color-nutri-glucose)",
        "var(--color-nutri-lactose)",
        "var(--color-nutri-maltose)",
        "var(--color-nutri-saccharose)",
        "var(--color-nutri-amidon)",
      ],
      dataLabels: {
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return w.config.series[seriesIndex]
        }
      },
      stroke: {
        width: 0
      }
    }
  }

  ngOnInit() {
  }

}
