import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'meal-list',
    loadChildren: () => import('./meal/meal-list/meal-list.module').then( m => m.MealListPageModule)
  },
  {
    path: 'meal-list/meal-tab',
    loadChildren: () => import('./meal/meal-tabnav/meal-tabnav.module').then(m => m.MealTabnavPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
