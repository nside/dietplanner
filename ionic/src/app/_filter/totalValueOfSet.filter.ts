import { Pipe, PipeTransform, Injectable } from '@angular/core'

@Pipe({name: 'totalValuesOfSetFilter'})

@Injectable()

export class TotalValuesOfSetFilterPipe implements PipeTransform {
  transform(set: [{name: string, value: number}]): number{
    return set.reduce((acc, current) => acc + current.value, 0);
  }
}