import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {

  @Input() showDetails: boolean
  public dataAggregate: any
  constructor() { }

  ngOnInit() {
    this.dataAggregate = {
      glucide: [
        {name: "Carrots", value: 25},
        {name: "Salmon", value: 42},
        {name: "Eggs", value: 64},
        {name: "Spinach", value: 18},
        {name: "Chicken", value: 48},
        {name: "Quinoa", value: 32},
        {name: "Broccoli", value: 55},
        {name: "Beef", value: 72},
        {name: "Lentils", value: 39},
        {name: "Apples", value: 22},
        {name: "Almonds", value: 31},
        {name: "Tuna", value: 45},
        {name: "Sweet potatoes", value: 36},
        {name: "Yogurt", value: 50}
      ]
    }
  }

  public aggregateValueSet(set: [{name: string, value: number}]){
    return set.reduce((acc, current) => acc + current.value, 0);
  }

}
