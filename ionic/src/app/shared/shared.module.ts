import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { DetailsComponent } from './details/details.component';
import { FilterModule } from '../filter.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FilterModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    DetailsComponent,
  ],
  bootstrap: [],
  declarations: [
    HeaderComponent,
    DetailsComponent,
  ]
})
export class SharedModule { }
