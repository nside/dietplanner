import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() public backable!: boolean
  @Input() public defaultHref!: string
  @Input() public customHeader!: string
  public navId!: string

  constructor(private activatedRoute: ActivatedRoute) { 
  }

  ngOnInit() {
    this.navId = this.activatedRoute.snapshot.paramMap.get('planId') as string  || ""
  }
}
