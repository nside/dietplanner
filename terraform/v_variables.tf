# Generales variables
variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "settings" {
  description = "Configuration settings"
  type        = map(any)
  default = {
    "database" = {      
      engine              = "aurora-postgresql"       
      engine_mode         = "provisioned"       
      engine_version      = "14.6"      
      instance_class      = "db.serverless" 
      db_name             = "dietplanner"    
      skip_final_snapshot = true
    },
    "bastion" = {
      count         = 1          
      instance_type = "t2.micro" 
      key_name      = "ben"
    }
  }
}

variable "public_subnet_cidr_blocks" {
  description = "Available CIDR blocks for public subnets"
  type        = list(string)
  default = [
    "10.0.1.0/24",
  ]
}

variable "private_subnet_cidr_blocks" {
  description = "Available CIDR blocks for private subnets"
  type        = list(string)
  default = [
    "10.0.101.0/24",
    "10.0.102.0/24",
  ]
}

# Secrets setting
variable "my_ip" {
  description = "Your IP address"
  type        = string
  sensitive   = true
}

variable "db_username" {
  description = "Database master user"
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "Database master user password"
  type        = string
  sensitive   = true
}