resource "aws_vpc" "dietplanner_vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name = "dietplanner_vpc"
  }
}

resource "aws_internet_gateway" "dietplanner_igw" {
  vpc_id = aws_vpc.dietplanner_vpc.id

  tags = {
    Name = "dietplanner_igw"
  }
}

resource "aws_subnet" "dietplanner_public_subnet" {
  vpc_id            = aws_vpc.dietplanner_vpc.id
  cidr_block        = var.public_subnet_cidr_blocks[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  count             = length(var.public_subnet_cidr_blocks)
  tags = {
    Name = "dietplanner_public_subnet_${count.index}"
  }
}

resource "aws_subnet" "dietplanner_private_subnet" {
  vpc_id            = aws_vpc.dietplanner_vpc.id
  cidr_block        = var.private_subnet_cidr_blocks[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  count             = length(var.private_subnet_cidr_blocks)
  tags = {
    Name = "dietplanner_private_subnet_${count.index}"
  }
}

resource "aws_route_table" "dietplanner_public_rt" {
  vpc_id = aws_vpc.dietplanner_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dietplanner_igw.id
  }
}

resource "aws_route_table_association" "public" {
  route_table_id = aws_route_table.dietplanner_public_rt.id
  subnet_id      = 	aws_subnet.dietplanner_public_subnet[count.index].id

  count          = length(var.public_subnet_cidr_blocks)
}

resource "aws_route_table" "dietplanner_private_rt" {
  vpc_id = aws_vpc.dietplanner_vpc.id
}

resource "aws_route_table_association" "private" {
  route_table_id = aws_route_table.dietplanner_private_rt.id
  subnet_id      = aws_subnet.dietplanner_private_subnet[count.index].id
  
  count          = length(var.private_subnet_cidr_blocks)
}