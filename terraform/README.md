# Terraform Configuration for AWS VPC, EC2 and RDS Aurora PostgreSQL
### /!\ In this scenario, only aurora will be billing for the first 12 Months of freetier account


### This project contains terraform configuration files on creating EC2 and RDS instances inside a Custom VPC on AWS. Here is the architecture of what will be created:

![Custom VPC architecture for AWS](./src/schema.png)

## Set Up
### Prerequisites
- [AWS IAM User](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-prereqs.html) Configure IAM User for terraform to get access key ID + secret access key /!\ Never use root account
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) installed and configuration
- [Terraform](https://www.terraform.io/downloads) installed
- [Terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/) installed
- [AWS S3] Configure S3 bucket for remote state in according configuration of terragrunt.hcl

### Create the Secrets file
Create a secrets file called ***v_secrets.tfvars*** and populate it with the follow secrets:
  - **db_username** <-- this is going to be the master user for RDS
  - **db_password** <-- this is going to be the RDS master user's password
  - **my_ip** <-- this is going to be your public IP

### SSH Key
  - You need "ed25519" format ssh key to connect to EC2
  - Push it manualy on Key Pair managmenent (EC2 service)
  - Set the name of the key added in **setting.bastion.key_name**  of ***v_variables.tf***

### S3 bucket 
  - Configure S3 bucket for terraform state
  - Configure terragrunt.hcl remote state conf accordly

### IAM Permission
This is the minimal permission needed by this terraform to run 
![Custom Permission for automated AWS users ](./src/iam.png)

## Running the Configuration
### Initializing the Terraform directory
Run the command: `terragrunt init`

### Apply the Terraform Config to AWS
Run the command: `terragrunt apply`

### To destroy everything that was created by the Terraform Config
Run the command: `terragrunt destroy`