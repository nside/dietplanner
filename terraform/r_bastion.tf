resource "aws_security_group" "dietplanner_bastion_sg" {
  name        = "dietplanner_bastion_sg"
  description = "Security group for dietplanner bastion"
  vpc_id      = aws_vpc.dietplanner_vpc.id

  ingress {
    description = "Allow SSH from my computer"
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["${var.my_ip}/32"]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "dietplanner_bastion_sg"
  }
}

resource "aws_instance" "dietplanner_bastion" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.settings.bastion.instance_type
  subnet_id              = aws_subnet.dietplanner_public_subnet[count.index].id
  key_name               = var.settings.bastion.key_name
  vpc_security_group_ids = [aws_security_group.dietplanner_bastion_sg.id]

  count                  = var.settings.bastion.count
  tags = {
    Name = "dietplanner_bastion_${count.index}"
  }
}

resource "aws_eip" "dietplanner_bastion_eip" {
  instance = aws_instance.dietplanner_bastion[count.index].id
  vpc      = true

  count    = var.settings.bastion.count
  tags = {
    Name = "dietplanner_bastion_eip_${count.index}"
  }
}