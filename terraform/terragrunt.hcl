remote_state {
  backend = "s3"
  config = {
    bucket = "dietplanner.nside.com"
    key    = "terraform-state"
    region = "eu-west-3"
  }
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF

terraform {
  required_version = ">=1.3.3"
  required_providers {
    aws = {
        source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "eu-west-3"
}

EOF
}

terraform {
  extra_arguments "custom_vars" {
    commands = get_terraform_commands_that_need_vars()

    optional_var_files = [
      "./v_secrets.tfvars",
    ]
  }

  extra_arguments "custom_vars" {
    commands = [
      "apply",
      "import",
      "push",
      "destroy"
    ]
    arguments = [
      "-auto-approve",
      "--refresh=true"
    ]
  }
}

retryable_errors = [
]