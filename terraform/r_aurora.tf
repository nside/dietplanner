resource "aws_security_group" "dietplanner_db_sg" {
  name        = "dietplanner_db_sg"
  description = "Security group for dietplanner databases"
  vpc_id      = aws_vpc.dietplanner_vpc.id

  ingress {
    description     = "Allow PostgreSQL traffic from only the bastion"
    from_port       = "5432"
    to_port         = "5432"
    protocol        = "tcp"
    security_groups = [aws_security_group.dietplanner_bastion_sg.id]
  }

  tags = {
    Name = "dietplanner_db_sg"
  }
}

resource "aws_db_subnet_group" "dietplanner_db_subnet_group" {
  name        = "dietplanner_db_subnet_group"
  description = "DB subnet group for dietplanner"
 
  subnet_ids  = [for subnet in aws_subnet.dietplanner_private_subnet : subnet.id]
}

resource "aws_rds_cluster" "dietplanner_db" {
  cluster_identifier          = "dietplanner-db"
  engine                      = var.settings.database.engine
  engine_mode                 = var.settings.database.engine_mode
  engine_version              = var.settings.database.engine_version
  database_name               = var.settings.database.db_name
  master_username             = var.db_username
  master_password             = var.db_password
  db_subnet_group_name        = aws_db_subnet_group.dietplanner_db_subnet_group.id
  vpc_security_group_ids      =  [aws_security_group.dietplanner_db_sg.id]
  allow_major_version_upgrade = true
  apply_immediately           = true
  skip_final_snapshot         = true

  serverlessv2_scaling_configuration {
    min_capacity = 0.5
    max_capacity = 1
  }
}

resource "aws_rds_cluster_instance" "dietplanner_db" {
  cluster_identifier  = aws_rds_cluster.dietplanner_db.id
  instance_class      = "db.serverless"
  engine              = aws_rds_cluster.dietplanner_db.engine
  engine_version      = aws_rds_cluster.dietplanner_db.engine_version
}